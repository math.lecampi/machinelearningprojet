import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier

def init_donnee_apprentissage():
    names = ['fixed acidity','volatile acidity','citric acid','residual sugar','chlorides','free sulfur dioxide','total sulfur dioxide','density','pH','sulphates','alcohol','quality']

    dataset = pd.read_csv("./projetmachinelearning/winequality-red.csv", names = names)

    X = dataset.iloc[:, :-1].values
    y = dataset.iloc[:, 11].values

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.20)

    scaler = StandardScaler()
    scaler.fit(X_train)

    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    error = []
    for k in range(1, 100):
        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(X_train, y_train)
        pred_k = knn.predict(X_test)
        error.append(np.mean(pred_k != y_test))

    # Affichage du taux d'erreur en fonction de k en utilisant matplotlib
    plt.figure(figsize=(12, 6))
    plt.plot(range(1, 100), error, color='red', linestyle='dashed', marker='o',
            markerfacecolor='blue', markersize=10)
    plt.title('Error Rate K Value')
    plt.xlabel('K Value')
    plt.ylabel('Mean Error')
    #plt.show(block=True)

    plt.savefig('./static/tauxErreur.png')

    return [X_train,y_train]