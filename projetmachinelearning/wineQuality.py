import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, confusion_matrix

#fixed acidity,volatile acidity,citric acid,residual sugar,chlorides,free sulfur dioxide,total sulfur dioxide,density,pH,sulphates,alcohol,quality;
names = ['fixed acidity','volatile acidity','citric acid','residual sugar','chlorides','free sulfur dioxide','total sulfur dioxide','density','pH','sulphates','alcohol','quality']

dataset = pd.read_csv("./winequality-red.csv", names = names)

X = dataset.iloc[:, :-1].values
y = dataset.iloc[:, 11].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.15)

# Affichage des données d'apprentissage
print(X_train)
# Affichage des données de test
print(X_test)

scaler = StandardScaler()
scaler.fit(X_train)

X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

# Affichage des données d'apprentissage normalisées
print(X_train)
# Affichage des données de test normalisées
print(X_test)


classifier = KNeighborsClassifier(n_neighbors=12)
classifier.fit(X_train, y_train)


#X_test2 = [[10.3,3.32,0.45,6.4,0.073,6,13,0.9976,3.23,0.82,12.6]]
########################
# Prédictions du modèle à partir des données de test
y_pred = classifier.predict(X_test)

print("--- Prediction ---")
print(y_pred)

print("--- Matrice de confusion ---")
print(confusion_matrix(y_test, y_pred))

print("--- F1 Score ---")
print(f1_score(y_test, y_pred, average=None))

########################
# Trouver la meilleure valeur de k à partir du taux d'erreur

error = []
for k in range(1, 100):
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)
    pred_k = knn.predict(X_test)
    error.append(np.mean(pred_k != y_test))

# Affichage du taux d'erreur en fonction de k en utilisant matplotlib
plt.figure(figsize=(12, 6))
plt.plot(range(1, 100), error, color='red', linestyle='dashed', marker='o',
         markerfacecolor='blue', markersize=10)
plt.title('Error Rate K Value')
plt.xlabel('K Value')
plt.ylabel('Mean Error')
#plt.show(block=True)

plt.savefig('tauxErreur.png')