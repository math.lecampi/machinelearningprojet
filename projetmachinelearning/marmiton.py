# -*- coding: utf-8 -*-
# Python 3

import requests as rq
from bs4 import BeautifulSoup
import pprint
import matplotlib
import matplotlib.pyplot as plt
import sqlite3 as sql
from collections import Counter
from sklearn.neighbors import KNeighborsClassifier

import pandas as pd

base_url = "https://www.marmiton.org/recettes/"
recettes = []
typeEntrees = "entree"
typePlats = "platprincipal"
typeDesserts = "dessert"

difficulte1 = "très facile"
difficulte2 = "facile"
difficulte3 = "Niveau moyen"
difficulte4 = "difficile"
prix1 = "bon marché"
prix2 = "Coût moyen"
prix3 = "assez cher"

def get_entree_plat_dessert_recettes():
    get_recettes(typeEntrees)
    get_recettes(typePlats)
    get_recettes(typeDesserts)

def get_recettes(type):
    r = rq.get(base_url + "?type=" + type)
    soup = BeautifulSoup(r.text, 'html.parser')
    url_recettes = soup.find_all("a", {"class": "recipe-card-link"})
    for url_recette in url_recettes:
        if url_recette.get('href'):
            a_url = url_recette.get('href')
            if a_url[0] == "/":
                a_url = base_url + a_url
            get_recette_detail(a_url)

def get_recette_detail(url):
    nbCommentaire = ""
    nbAjoutCarnet = ""
    nbPartage = ""
    temps = ""
    niveau = ""
    budget = ""
    note = ""

    r = rq.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')

    # Titre
    titre = soup.find("h1", {"class": "main-title"}).text
    
    # Commentaires
    nbCommentaire = int(soup.find("span", {"class": "recipe-infos-users__value mrtn-hide-on-print"}).text.split()[0])
    
    # Temps
    temps = soup.find("span", {"class": "title-2 recipe-infos__total-time__value"}).text.replace(" ","")
    if temps.find('h') != -1:
        tabTemps = temps.split('h')
        temps = int(tabTemps[0])*60
        if len(tabTemps) > 1 and tabTemps[1] != "":
            temps += int(tabTemps[1])
    else:
        temps = int(temps.split("min")[0])

    # Ajout carnet
    ajoutCarnet_div = soup.find("div", {"class": "recipe-infos-users__notebook"})
    if ajoutCarnet_div:
        nbAjoutCarnet = ajoutCarnet_div.find("span", {"class": "recipe-infos-users__value"}).text
        if nbAjoutCarnet == "":
            nbAjoutCarnet = 0
        else:
            if nbAjoutCarnet.find('k') != -1:
                nbAjoutCarnet = float(nbAjoutCarnet[:-1])*1000
            else:
                nbAjoutCarnet = float(nbAjoutCarnet)
    else:
        nbAjoutCarnet = 0

    # Partage
    partage_div = soup.find("div", {"class": "recipe-infos-users__share"})
    if partage_div:
        nbPartage = partage_div.find("span", {"class": "recipe-infos-users__value"}).text
        if nbPartage == "":
            nbPartage = 0
        else:
            if nbPartage.find('k') != -1:
                nbPartage = float(nbPartage[:-1])*1000
            else:
                nbPartage = float(nbPartage)
    else:
        nbPartage = 0
    
    # Niveau
    niveau_div = soup.find("div", {"class": "recipe-infos__level"})
    if niveau_div:
        niveau = niveau_div.find("span", {"class": "recipe-infos__item-title"}).text
        if niveau == difficulte1:
            niveau = 1
        elif niveau == difficulte2:
            niveau = 2
        elif niveau == difficulte3:
            niveau = 3
        elif niveau == difficulte4:
            niveau = 4
        else:
            niveau = 0
    
    # Budget
    budget_div = soup.find("div", {"class": "recipe-infos__budget"})
    if budget_div:
        budget = budget_div.find("span", {"class": "recipe-infos__item-title"}).text
        if budget == prix1:
            budget = 1
        elif budget == prix2:
            budget = 2
        elif budget == prix3:
            budget = 3
        else:
            budget = 0
    
    # Notes
    if soup.find("span", {"class": "recipe-reviews-list__review__head__infos__rating__value"}):
        note = float(soup.find("span", {"class": "recipe-reviews-list__review__head__infos__rating__value"}).text)
    else: note = 0
    
    recettes.append({"titre": titre, 
                    "commentaire": nbCommentaire, 
                    "ajoutCarnet": nbAjoutCarnet, 
                    "partage": nbPartage, 
                    "temps": temps, 
                    "niveau": niveau, 
                    "budget":budget, 
                    "url":url, 
                    "note": note})

get_entree_plat_dessert_recettes()
pp = pprint.PrettyPrinter(indent=4)
# pp.pprint(recettes)

# RECHERCHE #

tab_ajout = {"inf_10k" : 0, "10k_20k" : 0, "20k_30k" : 0, "30k_40k" : 0, "40k_50k" : 0, "sup_50k": 0}
tab_partage = {"inf_200": 0, "200_400": 0, "400_600": 0, "600_800": 0, "sup_800": 0}
total_partage = 0
for recette in recettes:
    if recette["ajoutCarnet"] < 10000 :
        tab_ajout["inf_10k"] += 1
    elif recette["ajoutCarnet"] >= 10000 and recette["ajoutCarnet"] < 20000:
        tab_ajout["10k_20k"] += 1
    elif recette["ajoutCarnet"] >= 20000 and recette["ajoutCarnet"] < 30000:
        tab_ajout["20k_30k"] += 1
    elif recette["ajoutCarnet"] >= 30000 and recette["ajoutCarnet"] < 40000:
        tab_ajout["30k_40k"] += 1
    elif recette["ajoutCarnet"] >= 40000 and recette["ajoutCarnet"] < 50000:
        tab_ajout["40k_50k"] += 1
    else:
        tab_ajout["sup_50k"] += 1

    if recette["partage"] < 200 :
        tab_partage["inf_200"] += 1
    elif recette["partage"] >= 200 and recette["partage"] < 400:
        tab_partage["200_400"] += 1
    elif recette["partage"] >= 400 and recette["partage"] < 600:
        tab_partage["400_600"] += 1
    elif recette["partage"] >= 600 and recette["partage"] < 800:
        tab_partage["600_800"] += 1
    else:
        tab_partage["sup_800"] += 1
    

# pp.pprint(tab_ajout)
# pp.pprint(tab_partage)

# GRAPH #

def graph_partage_carnet():
    dataX = []
    dataY = []
    labels = []
    colors = []
    for recette in recettes:
        dataX.append(recette["partage"])
        dataY.append(recette["ajoutCarnet"])
        labels.append(recette["titre"])
        note = recette["note"]
        if note < 4.6:
            colors.append("red")
        elif note >= 4.6 and note < 4.8:
            colors.append("yellow")
        else:
            colors.append("green") 
    
    plt.scatter(dataX, dataY, color=colors)

    plt.title("Partage x ajout carnet")
    plt.xlabel("Partage")
    plt.ylabel("Ajout carnet")
    plt.show()

def graph_partage_commentaire():
    dataX = []
    dataY = []
    labels = []
    colors = []
    for recette in recettes:
        dataX.append(recette["partage"])
        dataY.append(recette["commentaire"])
        labels.append(recette["titre"])
        note = recette["note"]
        if note < 4.6:
            colors.append("red")
        elif note >= 4.6 and note < 4.8:
            colors.append("yellow")
        else:
            colors.append("green") 
    
    plt.scatter(dataX, dataY, color=colors)

    plt.title("Partage x commentaire")
    plt.xlabel("Partage")
    plt.ylabel("Commentaire")
    plt.show()

def graph_commentaire_carnet():
    dataX = []
    dataY = []
    labels = []
    colors = []
    for recette in recettes:
        dataX.append(recette["commentaire"])
        dataY.append(recette["ajoutCarnet"])
        labels.append(recette["titre"])
        note = recette["note"]
        if note < 4.6:
            colors.append("red")
        elif note >= 4.6 and note < 4.8:
            colors.append("yellow")
        else:
            colors.append("green") 
    
    plt.scatter(dataX, dataY, color=colors)

    plt.title("Commentaire x ajout carnet")
    plt.xlabel("Commentaire")
    plt.ylabel("Ajout carnet")
    plt.show()

def graph_partage_temps():
    dataX = []
    dataY = []
    labels = []
    colors = []
    for recette in recettes:
        dataX.append(recette["partage"])
        dataY.append(recette["temps"])
        labels.append(recette["titre"])
        note = recette["note"]
        if note < 4.6:
            colors.append("red")
        elif note >= 4.6 and note < 4.8:
            colors.append("yellow")
        else:
            colors.append("green")
    
    plt.scatter(dataX, dataY, color=colors)

    plt.title("Partage x temps")
    plt.xlabel("Partage")
    plt.ylabel("Temps")
    plt.show()

def graph_temps_carnet():
    dataX = []
    dataY = []
    labels = []
    colors = []
    for recette in recettes:
        dataX.append(recette["temps"])
        dataY.append(recette["ajoutCarnet"])
        labels.append(recette["titre"])
        note = recette["note"]
        if note < 4.6:
            colors.append("red")
        elif note >= 4.6 and note < 4.8:
            colors.append("yellow")
        else:
            colors.append("green") 
    
    plt.scatter(dataX, dataY, color=colors)

    plt.title("Temps x ajout carnet")
    plt.xlabel("Temps")
    plt.ylabel("Ajout carnet")
    plt.show()

def graph_temps_niveau():
    dataX = []
    dataY = []
    labels = []
    colors = []
    for recette in recettes:
        dataX.append(recette["temps"])
        dataY.append(recette["niveau"])
        labels.append(recette["titre"])
        note = recette["note"]
        if note < 4.6:
            colors.append("red")
        elif note >= 4.6 and note < 4.8:
            colors.append("yellow")
        else:
            colors.append("green") 
    
    plt.scatter(dataX, dataY, color=colors)

    plt.title("Temps x niveau")
    plt.xlabel("Temps")
    plt.ylabel("Niveau")
    plt.show()

def graph_budget_temps():
    dataX = []
    dataY = []
    labels = []
    colors = []
    for recette in recettes:
        dataX.append(recette["budget"])
        dataY.append(recette["temps"])
        labels.append(recette["titre"])
        note = recette["note"]
        if note < 4.6:
            colors.append("red")
        elif note >= 4.6 and note < 4.8:
            colors.append("yellow")
        else:
            colors.append("green")
    
    plt.scatter(dataX, dataY, color=colors)

    plt.title("Budget x temps")
    plt.xlabel("Budget")
    plt.ylabel("Temps")
    plt.show()

graph_partage_carnet()
graph_partage_commentaire()
graph_commentaire_carnet()
graph_partage_temps()
graph_temps_carnet()
graph_temps_niveau()
graph_budget_temps()


con = sql.connect('recette.bd',isolation_level=None)
cur = con.cursor()
cur.execute("CREATE TABLE recette (id INTEGER PRIMARY KEY AUTOINCREMENT, titre STRING ,commentaire INTEGER, ajoutCarnet FLOAT, partage FLOAT, temps INTEGER, niveau INTEGER, budget INTEGER, url STRING ,note FLOAT)")

for recette in recettes:
    cur.execute("INSERT INTO recette VALUES (?,?,?,?,?,?,?,?,?,?)", (None,recette['titre'],recette['commentaire'],recette['ajoutCarnet'],recette['partage'],recette['temps'],recette['niveau'],recette['budget'],recette['url'],recette['note']))
    
con.commit()

df = pd.DataFrame(recettes, columns = ['titre' ,'commentaire' , 'ajoutCarnet' , 'partage' , 'temps' , 'niveau' , 'budget' , 'url'  ,'note'])
df.to_csv(r'./recettes.csv',index = False)
