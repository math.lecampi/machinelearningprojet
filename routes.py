from middleware import *

def init_DB(app):
    app.add_url_rule('/api/initdb', 'initDb', init_db, methods=['GET'])

def fill_DB(app):
    app.add_url_rule('/api/filldb', 'fillDb', fill_db, methods=['GET'])

def init_routes_api(app):
    app.add_url_rule('/', 'home', index, methods=['GET'])
    app.add_url_rule('/api/wine', 'api_get_wine', api_get_wine, methods=['GET'])
    app.add_url_rule('/wine', 'get_wine', get_wine, methods=['GET'])
    app.add_url_rule('/test-wine', 'test_wine', test_wine, methods=['GET', 'POST'])

def init_error_handler(app):
    app.errorhandler(404)(error_404)
    app.errorhandler(500)(error_500)
    app.add_url_rule('/crash', 'crash', crash, methods=['GET'])