from flask import Flask, jsonify, abort, make_response, render_template, flash, request, send_file
from provider_service import *
from init_donnee import *
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, confusion_matrix
import os

db_engine = 'sqlite:///./database.db'
DATA_PROVIDER = DataProviderService(db_engine)
donnee_apprentissage = init_donnee_apprentissage()

def index():
    return render_template('about.html')

def init_db():
    DATA_PROVIDER.init_database()
    return jsonify({"response": "init OK"})

def fill_db():
    DATA_PROVIDER.fill_database()
    return jsonify({"response": "fill OK"})

def api_get_wine():
    wines = DATA_PROVIDER.get_wine(serialize=True)
    if wines:
        return jsonify(wines)
    else:
        abort(404)

def get_wine():
    wines = DATA_PROVIDER.get_wine(serialize=True)
    return render_template('wine.html', wines=wines)

def test_wine():
    if request.method == 'POST':
        classifier = KNeighborsClassifier(n_neighbors=int(request.form["neighbors"]))
        classifier.fit(donnee_apprentissage[0], donnee_apprentissage[1])

        # Prédictions du modèle à partir des données de test
        y_pred = classifier.predict([[request.form['fixed_acidity'],request.form['volatile_acidity'],request.form['citric_acid'],request.form['residual_sugar'],request.form['chlorides'],request.form['free_sulfur_dioxide'],request.form['total_sulfur_dioxide'],request.form['density'],request.form['pH'],request.form['sulphates'],request.form['alcohol']]])

        result = y_pred[0]
        return render_template("test_wine.html", result=result)
    return render_template("test_wine.html")

def crash():
    abort(500)

# ERROR
def error_404(error):
    flash("Sorry, error 404", "error")
    return render_template('404.html')

def error_500(error):
    flash("Sorry, error 500", "error")
    return render_template('500.html')
