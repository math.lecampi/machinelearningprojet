from sqlalchemy import Column, Integer, Float
from sqlalchemy.orm import relationship
from models.Model import Model

class Wine(Model):
    __tablename__ = 'wine'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    fixed_acidity = Column(Float, nullable=False)
    volatile_acidity = Column(Float, nullable=False)
    citric_acid = Column(Float, nullable=False)
    residual_sugar = Column(Float, nullable=False)
    chlorides = Column(Float, nullable=False)
    free_sulfur_dioxide = Column(Float, nullable=False)
    total_sulfur_dioxide = Column(Float, nullable=False)
    density = Column(Float, nullable=False)
    pH = Column(Float, nullable=False)
    sulphates = Column(Float, nullable=False)
    alcohol = Column(Float, nullable=False)
    quality = Column(Float, nullable=False)

    def serialize(self):
        return {
            'id':self.id,
            'fixed_acidity':self.fixed_acidity,
            'volatile_acidity':self.volatile_acidity,
            'citric_acid':self.citric_acid,
            'residual_sugar':self.residual_sugar,
            'free_sulfur_dioxide':self.free_sulfur_dioxide,
            'total_sulfur_dioxide':self.total_sulfur_dioxide,
            'density':self.density,
            'pH': self.pH,
            'sulphates': self.sulphates,
            'alcohol': self.alcohol,
            'quality': self.quality
        }