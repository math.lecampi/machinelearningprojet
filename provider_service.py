from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

import datetime

from models.Wine import Wine
from models.initDB import init_database
import pandas as pd

class DataProviderService:
    def __init__(self, engine):
        """
        :param engine: The engine route and login details
        :return: a new instance of DAL class
        :type engine: string
        """
        if not engine:
            raise ValueError('The values specified in engine parameter has to be supported by SQLAlchemy')
        self.engine = engine
        db_engine = create_engine(engine)
        db_session = sessionmaker(bind=db_engine)
        self.session = db_session()

    def init_database(self):
        """
        Initializes the database tables and relationships
        :return: None
        """
        init_database(self.engine)

    def get_wine(self, id=None, serialize=False):
        all_wines = []

        if id is None:
            all_wines = self.session.query(Wine).all()
        else:
            all_wines = self.session.query(Wine).filter(Wine.id == id).all()

        if serialize:
            return [wine.serialize() for wine in all_wines]
        else:
            return all_wines
        

    def fill_database(self):
        names = ['fixed_acidity','volatile_acidity','citric_acid','residual_sugar','chlorides','free_sulfur_dioxide','total_sulfur_dioxide','density','pH','sulphates','alcohol','quality']
        dataset = pd.read_csv("./projetmachinelearning/winequality-red.csv",names=names)
        for (index, line) in dataset.iterrows():
            wine = Wine(fixed_acidity = line[0],
                        volatile_acidity = line[1],
                        citric_acid = line[2],
                        residual_sugar = line[3],
                        chlorides = line[4],
                        free_sulfur_dioxide = line[5],
                        total_sulfur_dioxide = line[6],
                        density = line[7],
                        pH = line[8],
                        sulphates = line[9],
                        alcohol = line[10],
                        quality = line[11])
            self.session.add(wine)
            self.session.commit()