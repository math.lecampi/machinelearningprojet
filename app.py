from flask import Flask, jsonify, abort, make_response, render_template
from routes import *

app = Flask(__name__)

init_routes_api(app)
init_DB(app)
fill_DB(app)
init_error_handler(app)

@app.template_filter('color')
def color(text):
    color = 'black'
    if text == 1:
        color = 'black'
    elif text == 2:
        color = 'brown'
    elif text == 3:
        color = 'purple'
    elif text == 4:
        color = 'red'
    elif text == 5:
        color = 'orange'
    elif text == 6:
        color = 'gold'
    elif text == 7:
        color = 'cyan'
    elif text == 8:
        color = 'darkcyan'
    elif text == 9:
        color = 'blue'
    elif text == 10:
        color = 'green'
    return "<span style='color:" + color + "'>" + str(text) + "</span> / 10 "


if __name__ == "__main__":
    app.secret_key = 'lealefeuvre'
    app.run(debug=True)
